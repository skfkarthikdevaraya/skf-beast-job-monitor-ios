


function onLoad() {
    document.addEventListener("deviceready", onDeviceReady(), false);
}

function onDeviceReady() {
    checkConnection();
    updateTimeStamp ();
    
}

/* Function Check Internet Connection */
function checkConnection() {
    if (navigator.connection.type == 'none') {
        navigator.notification.alert("This application requires internet.", null, "Connection Error", "OK");
    }
}

function updateTimeStamp() {
    var time =  (new Date).getTime();
    var millisec = Date.now();
    var dtFromMillisec = new Date(millisec);
    var update = "Last updated on: ";
    $("#timeStamp").html(update);
    $("#timeStamp").html(update + dtFromMillisec);
    $("timeStamp").page();
}



