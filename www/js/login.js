
var username;
var password;

function LoginButtonClicked () {
    
     username = $("#username").val();
     password = $("#password").val();
    
    // To remove the  blank space in the textfields before submitting
    username = $.trim(username);
    password = $.trim(password);
    
    if(username =="" || password ==""){
        
       validationcheck();
        //getData();
        //window.location.href = "#mainPage";
        
    }else {
        var newuser =(username.replace('@skf.com', ''));
        newuser = newuser.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                                return letter.toUpperCase();
                                                });
        $("#welcomeText").html(newuser);
        $("#infoUser").html(newuser);
        
        $.ajax ({
                type: "POST",
                url: "http://gotx66.got.skf.se/dea-xml/index.php",
                dataType: "xml",
                async: false,
                data: '{}',
                timeout:10000,
                beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", "Basic "+btoa(username + ":" + password));},
                success: function (){
                    getData ();
                    window.location.href = "#mainPage";
                },
                error: function(xhr, status, error) {

                    if(xhr.status == 'timeout') {       // When app is connected to SKF network but server is down.
                    
                        navigator.notification.alert("Currently server is not responding. Please Try again later.", null, "Login Error", "OK");
                        location.reload(true);
                    
                    }else if(xhr.status == 404) {       // when app is not connected to SKF Network
                    
                        navigator.notification.alert("This application requires SKF Network. Please connent and try later.", null, "Login Error", "OK");
                        location.reload(true);
                    
                    }else if(xhr.status == 200 || xhr.status == 500 || xhr.status == 0){        // When app is connected to SKF Network with correct
                
                        navigator.notification.alert("Currently server is not responding. Please Try again later.", null, "Login Error", "OK");
                        location.reload(true);
                    
                    }else{                              // When user entered login credential is incorrect
                    
                        navigator.notification.alert("The username or password you entered is incorrect.", null, "Login Error", "OK");
                        location.reload(true);
                    }
                }
            });
        }
    }



function validationcheck ()
{
    navigator.notification.alert("You must enter username and password", null, "Login Error", "OK");
}

function logout ()
{
    navigator.notification.confirm( "Are you sure, you want to logout?", logoutCallback, "Logout", ["No", "Yes"]);
}

function logoutCallback(buttonIndex)
{
    if(buttonIndex == 2){
        window.location.href="#loginPage";
    }
}


