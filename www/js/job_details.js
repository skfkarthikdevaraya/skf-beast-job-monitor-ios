



function runlogButtonClicked (){

    $("#runlogIframe").contents().empty();
    
    var runlogURL = "http://gotx66.got.skf.se/dea/simfile.php";
    var jobNumber = selectedJobID;
    var fullURL_runlog = runlogURL + "?" + "id=" + jobNumber;
    var message="Data is not available in the Run Beast Server.";
    $.ajax({
           type: "POST",
           url:fullURL_runlog,
           dataType: "html",
           success: function(runLogData){
            var  dataParse = $.parseHTML(runLogData);
            var runlogText=   $(dataParse).find("PRE").text();
           
           var ifrm = document.getElementById('runlogIframe');
            ifrm.document.open();
    
           
           if(runlogText === ""){
                ifrm.document.write(message);
           }else {
                ifrm.document.write(runLogData);
            }
            ifrm.document.close();

           },
           error: function() {
                navigator.notification.alert("An error occurred while processing XML file.", null, "Runlog Error", "OK");
           }
        });
    
}

function simlogButtonClicked () {
    
    $("#simlogIframe").contents().empty();

    
    var simlogURL = "http://gotx66.got.skf.se/dea/simfile.php";
    var jobNumber = selectedJobID;
    var fullURL_simlog = simlogURL + "?" + "id=" + jobNumber +"&ft=simlog";
    var message="Data is not available in the Run Beast Server.";
    $.ajax({
           type: "POST",
           url:fullURL_simlog,
           dataType: "text",
           success: function(simLogData){
           
           var  dataParse = $.parseHTML(simLogData);
           var simlogText=   $(dataParse).find("PRE").text();


           var ifrm = document.getElementById('simlogIframe');
           ifrm.document.open();

                   if(simlogText === ""){
                        ifrm.document.write(message);
                        ifrm.document.close();
                   }else {
                        ifrm.document.write(simLogData);
                        ifrm.document.close();
                   }
           },
           error: function() {
           navigator.notification.alert("An error occurred while processing XML file.", null, "Simlog Error", "OK");
           }
        });
    
}

function stopJob ()  {
    
    if(jobState === 'Done'){
        navigator.notification.confirm( "The selected job is already completed. You can't stop it.", null, "Warning", "Ok");
    }else if(jobState === 'Undefined'){
        navigator.notification.confirm( "Sorry. Only Running jobs can be stopped.", null, "Warning", "Ok");
    }else if(jobState === 'Running'){
        navigator.notification.confirm( "Are you sure, you want to stop this job?. This job can't be undo later.", stopJobCallback, "Stop Job", ["No", "Yes"]);
    }
}

function stopJobCallback (buttonIndex) {
    
    if(buttonIndex == 2){
        var stopJobURL = "http://gotx66.got.skf.se/dea-xml/";
        var jobId = selectedJobID;
        var fullURL_stopJob = stopJobURL + "?task=kill" + "&data=" + jobId;
        var xmldata;
        $.ajax({
               type: "POST",
               url:fullURL_stopJob,
               dataType: "xml",
               async: false,
               beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", "Basic "+btoa(username + ":" + password));
               xmldata = xhr;
               },
               success: function(xml){
               var res = xmldata.responseText;
               var doc = $($.parseXML(res));
               var stops = doc.find('kill').text();
               if(stops === 'OK' ){  navigator.notification.confirm( "Selected job is stoped successfully.", stopJobSuccess, "Success", "Ok"); }
               },
               error: function(xhr, status, error) {
                    navigator.notification.alert("Currently we can't stop this job.", null, "Error", "OK");
               }
            });
        }
}


function stopJobSuccess () {
    window.location.href = "#mainPage";
}

function returnToHome () {
    
    window.location.href = "#mainPage";
    if(WhichState == 2){
        $('.completed_job_tab').addClass('ui-btn-active');
    }else{
        $('.running_job_tab').addClass('ui-btn-active');
    }
}

