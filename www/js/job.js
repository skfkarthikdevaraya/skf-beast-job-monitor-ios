
var myArray = [];
var selectedJobCaseID;
var selectedJobID;
var jobState;
var WhichState =  1;
var job_status;

jQuery(document).ready(function(event) {
                       
    if(WhichState == 1){
        $('.running_job_tab').addClass('ui-btn-active');
    }
    $('.running_job_tab').click(function() {
         WhichState = 1;
         $('.completed_job_tab').removeClass('ui-btn-active');
         $(this).addClass('ui-btn-active');
    });
   
   $('.completed_job_tab').click(function() {
         WhichState = 2;
         $('.running_job_tab').removeClass('ui-btn-active');
         $(this).addClass('ui-btn-active');

    });
});


function returnHome () {
    
    window.location.href = "#mainPage";
    if(WhichState == 2){
        $('.completed_job_tab').addClass('ui-btn-active');
    }else{
        $('.running_job_tab').addClass('ui-btn-active');
    }
}

function refreshButtonClicked () {
    
    //navigator.notification.confirm( "Refresh button clicked", null, "Refresh", "OK");
    reloadData ();
    updateTimeStamp ();

}

function reloadData () {
    

    getData();
    $("#done-job-list").listview("refresh");
    $("#running-job-list").listview("refresh");
}



function getData() {
    
//    var username = "prabhu.balachandran@skf.com";
//    var password = "pr4664";
//    var username = "nagendraprasad.b@skf.com";
//    var password = "NP6dc1";
    
    var xmldata;
    $.ajax ({
         type: "POST",
         url: "http://gotx66.got.skf.se/dea-xml/index.php",
         dataType: 'xml',
         async: false,
         data: '{}',
         beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", "Basic "+btoa(username + ":" + password));
         xmldata = xhr;},
         success: function (data){
            parseXML(xmldata.responseText);
         },
         error: function(xhr, status, error) {
         navigator.notification.alert("Currently server is not responding. Please Try again later.", null, "Failure Error", "OK");
            }
     });
}

function parseXML(xml) {
    
    var count = 0;
    var index = 0;
    
    $(xml).find("joblist").each(function() {
        count = 0;
        $(this).find("job").each(function() {
                                 
             var text = $(this).text();
             jobObject = new Object();
                                 
             jobObject.caseid = $(this).find("caseid").text();
             jobObject.status = $(this).find("status").text();
             jobObject.progress = $(this).find("progress").text();
             jobObject.steps = $(this).find("steps").text();
             jobObject.speedup = $(this).find("speedup").text();
             jobObject.efficiency = $(this).find("efficiency").text();
             jobObject.simtime = $(this).find("simtime").text();
             jobObject.timeleft = $(this).find("timeleft").text();
             jobObject.rwtime = $(this).find("rwtime").text();
             jobObject.info = $(this).find("info").text();
             jobObject.jobid = $(this).find("jobid").text();
                                 
             myArray[count] = jobObject;
             count++;
                                 
            });
        });
    
// To display the user's Running & Completed job lists
            $(xml).find('job').each(function() {
                
                job_status = ($(this).find("status").text());
                var job_progress = $(this).find("progress").text();
                var temp = (($(this).find("caseid").text()));
                var caseID  = $(this).find("caseid").text();
                var text = "ERROR - Simulation directory does not exist:";
                var myhtml="";
                                    
                if(job_status === 'Done'){
                                    
                        myhtml += '<li><a href="#" class="jobdetails-go" '+"onclick=goToJobDetails("+index+")>" + temp + ' </br> <span id="statusDoneColor"> '+ job_status +', '+ job_progress +' </span></a></li>';
                        $("#done-job-list").append(myhtml);
                }else{
                    if(job_status === 'Undefined'){
                        if(caseID == text){
                            myhtml ="";
                        }else{
                            myhtml += '<li><a href="#" class="jobdetails-go" '+"onclick=goToJobDetails("+index+")>" + temp + ' </br> <span id="statusUndefinedColor"> '+ job_status +', '+ job_progress +' </span></a></li>';
                        }
                    }else if(job_status === 'Running'){
                                    
                            myhtml += '<li><a href="#" class="jobdetails-go" '+"onclick=goToJobDetails("+index+")>" + temp + ' </br> <span id="statusRunningColor"> '+ job_status +', '+ job_progress +' </span></a></li>';
                    }
                            $("#running-job-list").append(myhtml);
                }
                index++;
            });
}

function goToJobDetails (index) {
    
    jobState = myArray[index].status; 
    selectedJobCaseID = myArray[index].caseid;
    selectedJobID= myArray[index].jobid;
    $("#selected-caseid").html(selectedJobCaseID);
    var jdhtml="";
    
    if(jobState === 'Done'){
        jdhtml += "</li><li>Status:"+myArray[index].status+"</li><li>Progress: "+myArray[index].progress+"</li><li>Steps: "+myArray[index].steps+"</li><li>Speed UP :"+myArray[index].speedup+"</li><li>Efficiency: "+myArray[index].efficiency+"</li><li>Sim Time: "+myArray[index].simtime+"</li><li>Time Left: "+myArray[index].timeleft+"</li><li>Run Time: "+myArray[index].rwtime+"</li><li>Info: "+myArray[index].info+"</li><li>JobID: "+myArray[index].jobid+"</li>";
    }else{
        if(jobState === 'Undefined'){
            jdhtml += "</li><li>Status: "+myArray[index].status+"</li><li>Progress: "+myArray[index].progress+"</li><li>Steps: "+myArray[index].steps+"</li><li>Speed UP :"+myArray[index].speedup+"</li><li>Efficiency: "+myArray[index].efficiency+"</li><li>Sim Time: "+myArray[index].simtime+"</li><li>Time Left: "+myArray[index].timeleft+"</li><li>Run Time: "+myArray[index].rwtime+"</li><li>Info: "+myArray[index].info+"</li><li>JobID: "+myArray[index].jobid+"</li>";
        }else if(jobState === 'Running'){
            jdhtml += "</li><li>Status: "+myArray[index].status+"</li><li>Progress: "+myArray[index].progress+"</li><li>Steps: "+myArray[index].steps+"</li><li>Speed UP :"+myArray[index].speedup+"</li><li>Efficiency: "+myArray[index].efficiency+"</li><li>Sim Time: "+myArray[index].simtime+"</li><li>Time Left: "+myArray[index].timeleft+"</li><li>Run Time: "+myArray[index].rwtime+"</li><li>Info: "+myArray[index].info+"</li><li>JobID: "+myArray[index].jobid+"</li>";
        }
    }
    $("#job_details_list").html(jdhtml);
    $.mobile.changePage("#jobDetails");
}




